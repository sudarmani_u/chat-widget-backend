var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

http.listen(3000, () => {
  console.log('listening on *:3000');
});

io.on('connection', (socket) => {
    
    console.log('Connect user');

    socket.on('disconnect',() => {
        console.log('disconnect user');
    });

    socket.on('chat-message',(data) => {
      socket.broadcast.emit('chat-message',(data))
    });

    socket.on('typing',(data) => {
      socket.broadcast.emit('typing',(data))
    });

    socket.on('typingStopped',(data) => {
      socket.broadcast.emit('typingStopped',(data))
    });

    socket.on('Joined',(data) => {
      socket.broadcast.emit('Joined',(data))
    });

    socket.on('Left',(data) => {
      socket.broadcast.emit('Left',(data))
    });

});